﻿# Infrastructure Management

ASP.NET Application that can be used to list resources (Servers, Websites, etc) within an operations team. Resources can have links to RDP, ILO and Web access. Lightweight and fast. No database, uses XML for data.

Requires .net framework 2.0

![screenshot01.png](https://bitbucket.org/repo/RKx5qy/images/4082177361-screenshot01.png)